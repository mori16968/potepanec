require 'rails_helper'

RSpec.describe Spree::ProductDecorator, type: :model do
  describe "関連商品取得メソッド" do
    let(:taxon) { create(:taxon) }
    let(:other_taxon) { create(:taxon, name: "Other Taxon") }
    let(:product) { create(:product, taxons: [taxon]) }
    let(:other_product) { create(:product, taxons: [other_taxon]) }

    it "関連商品が重複していないこと" do
      expect(product.related_products).to eq product.related_products.uniq
    end

    it "元の商品は含まれていないこと" do
      expect(product.related_products).not_to include(product)
    end

    it "関連しない商品（カテゴリーが異なる商品）は含まれていないこと" do
      expect(product.related_products).not_to include(other_product)
    end
  end
end
