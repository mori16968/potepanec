require 'rails_helper'

RSpec.describe "Products", type: :request do
  describe "商品詳細ページ" do
    let(:taxon) { create(:taxon) }
    let(:other_taxon) { create(:taxon, name: "Other Taxon") }
    let(:product) { create(:product, taxons: [taxon]) }
    let(:other_product) { create(:product, taxons: [other_taxon]) }
    let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }

    before do
      get potepan_product_path(product.id)
    end

    it "商品詳細ページのリクエストが成功すること" do
      expect(response).to have_http_status(200)
    end

    it "商品情報が正しく表示されていること" do
      expect(response.body).to include(product.name)
      expect(response.body).to include(product.display_price.to_s)
      expect(response.body).to include(product.description)
    end

    describe "関連商品部分" do
      it "5つの関連商品のうち4つ表示されていること" do
        expect(response.body).to include(related_products[0].name)
        expect(response.body).to include(related_products[1].name)
        expect(response.body).to include(related_products[2].name)
        expect(response.body).to include(related_products[3].name)
        expect(response.body).not_to include(related_products[4].name)
      end

      it "関連商品が重複していないこと" do
        expect(product.related_products).to eq product.related_products.uniq
      end

      it "元の商品は含まれていないこと" do
        expect(product.related_products).not_to include(product)
      end

      it "関連しない商品（カテゴリーが異なる商品）は表示されないこと" do
        expect(response.body).not_to include(other_product.name)
      end
    end
  end
end
