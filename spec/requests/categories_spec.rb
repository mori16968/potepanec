require 'rails_helper'

RSpec.describe "Categories", type: :request do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, parent_id: taxonomy.root.id) }
  let!(:product) { create(:product, taxons: [taxon]) }

  before do
    get potepan_category_path(taxon.id)
  end

  it "カテゴリーページのリクエストが成功すること" do
    expect(response).to have_http_status(200)
  end

  it "taxonomyの名前が表示されていること" do
    expect(response.body).to include taxonomy.name
  end

  it "taxonの名前が表示されていること" do
    expect(response.body).to include taxon.name
  end

  it "productが表示されていること" do
    expect(response.body).to include product.name
  end
end
