require 'rails_helper'

RSpec.feature "Cetegories", type: :feature do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, parent_id: taxonomy.root.id) }
  let(:other_taxon) { create(:taxon, name: "Other Taxon", parent_id: taxonomy.root.id) }
  let!(:product) { create(:product, taxons: [taxon]) }
  let!(:other_product) do
    create(:product, name: "Other Product",
                     price: "1234",
                     taxons: [other_taxon])
  end

  describe "商品カテゴリー部分" do
    before do
      visit potepan_category_path(taxon.id)
    end

    it "カテゴリー部分が正しく表示されていること" do
      within ".side-nav" do
        expect(page).to have_content taxonomy.name
        expect(page).to have_content "#{taxon.name}(#{taxon.products.count})"
      end
    end

    describe "カテゴリーをクリックした時" do
      before do
        within ".side-nav" do
          click_on "#{taxon.name}(#{taxon.products.count})"
        end
      end

      it "クリックされたカテゴリーに属する商品が、商品一覧部分に表示されること" do
        within ".productBox" do
          expect(page).to have_content product.name
          expect(page).to have_content product.display_price
        end
      end

      it "クリックされたカテゴリーに属さない商品が、商品一覧部分に表示されないこと" do
        within ".productBox" do
          expect(page).not_to have_content other_product.name
          expect(page).not_to have_content other_product.display_price
        end
      end
    end

    it "商品をクリックした時、商品詳細ページにアクセスできること" do
      click_on product.name
      expect(current_path).to eq potepan_product_path(product.id)
    end
  end
end
