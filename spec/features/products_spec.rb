require 'rails_helper'

RSpec.feature "Products", type: :feature do
  let(:taxon) { create(:taxon) }
  let(:product) { create(:product, taxons: [taxon]) }
  let!(:related_product) { create(:product, taxons: [taxon]) }

  describe "商品詳細ページ" do
    before do
      visit potepan_product_path(product.id)
    end

    it "商品情報が正しく表示されていること" do
      expect(page).to have_content product.name
      expect(page).to have_content product.display_price
      expect(page).to have_content product.description
    end

    describe "トップページリンク" do
      context "NAVBER ロゴ部分をクリックした時" do
        it "トップページに戻ること" do
          within ".navbar-header" do
            find(".navbar-brand").click
          end
          expect(page).to have_current_path "/potepan"
        end
      end

      context "NAVBER 右側Homeリンクをクリックした時" do
        it "トップページに戻ること" do
          within ".navbar-right" do
            click_link "Home"
          end
          expect(page).to have_current_path "/potepan"
        end
      end

      context "LIGHT SECTION 右側 商品名横Homeリンクをクリックした時" do
        it "トップページに戻ること" do
          within ".pageHeader" do
            click_link "Home"
          end
          expect(page).to have_current_path "/potepan"
        end
      end
    end

    it "商品詳細ページの一覧ページへ戻るリンクが正しく動作すること" do
      click_on "一覧ページへ戻る"
      expect(current_path).to eq potepan_category_path(taxon.id)
    end

    describe "関連商品部分" do
      it "関連商品が正しく表示されていること" do
        expect(page).to have_content related_product.name
        expect(page).to have_content related_product.display_price
      end

      it "関連商品をクリックした時、商品詳細ページにリンクしていること" do
        click_on related_product.name
        expect(current_path).to eq potepan_product_path(related_product.id)
      end
    end
  end
end
