require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe "full_title_helperのテスト" do
    describe "商品詳細ページ" do
      subject { full_title(product_name) }

      context "引数がある場合" do
        let(:product_name) { create(:product).name }

        it { is_expected.to eq "#{product_name} - BIGBAG Store" }
      end

      context "引数が空の場合" do
        let(:product_name) { "" }

        it { is_expected.to eq "BIGBAG Store" }
      end

      context "引数がnilの場合" do
        let(:product_name) { nil }

        it { is_expected.to eq "BIGBAG Store" }
      end
    end

    describe "カテゴリーページ" do
      subject { full_title(taxon_name) }

      context "引数がある場合" do
        let(:taxon_name) { create(:taxon).name }

        it { is_expected.to eq "#{taxon_name} - BIGBAG Store" }
      end

      context "引数が空の場合" do
        let(:taxon_name) { "" }

        it { is_expected.to eq "BIGBAG Store" }
      end

      context "引数がnilの場合" do
        let(:taxon_name) { nil }

        it { is_expected.to eq "BIGBAG Store" }
      end
    end
  end
end
